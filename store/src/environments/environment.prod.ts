/*
 * spurtcommerce
 * version 3.0
 * http://www.spurtcommerce.com
 *
 * Copyright (c) 2019 piccosoft ltd
 * Author piccosoft ltd <support@piccosoft.com>
 * Licensed under the MIT license.
 */
export const environment = {
    production: true,
    storeUrl: 'http://34.251.146.234:9000/api/', // <Your API base url>
    imageUrl: 'http://34.251.146.234:9000/api/media/image-resize' // <Your API url for image resize>
};
